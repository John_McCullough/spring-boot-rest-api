package com.citi.spring.rest;

import java.util.Collection;

import java.util.Optional;
import com.citi.spring.entities.Trade;
import com.citi.spring.service.TradeService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/trader")
@CrossOrigin
public class TraderController {
    
    @Autowired
    private TradeService tradeService;


    @RequestMapping(method=RequestMethod.GET)
    public Collection<Trade> getAllTrades(){
        return tradeService.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    //Optional keyword for an object that may or may not be null. E.g. if there retrieve
    public Optional<Trade> getTradeById(@PathVariable("id") String id) {
        return tradeService.getTradeById(new ObjectId(id));
	}

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteTrade(@PathVariable("id") String id) {
        tradeService.deleteTrade(new ObjectId(""+id));
    }

    @RequestMapping(method=RequestMethod.POST)
    public void createTrade(@RequestBody Trade t){
        tradeService.createTrade(t);
    }

}