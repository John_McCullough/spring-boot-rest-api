package com.citi.spring.service;

import com.citi.spring.data.TradeRepository;
import com.citi.spring.entities.Trade;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class TradeService {
    @Autowired
    private TradeRepository dao;

    public Collection<Trade> getAll() { return dao.findAll(); }

    public Optional<Trade> getTradeById(ObjectId id){
        return dao.findById(id);
	}

    public void deleteTrade(ObjectId id) {
        dao.deleteById(id);
    }

    public void createTrade(Trade t){
        dao.insert(t);
    }
}
